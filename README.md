# word-occurrence-counter

This project contains a word occurrence counter library, extendable with custom word providers, word processors and word occurrence counters.

The contained console application should be suitable for most basic applications, by supporting one to many files, to count words in and two word counters to choose from.

The library has two word counters, a "slow" sequential counter and a "faster" map reduce counter working with Tasks.