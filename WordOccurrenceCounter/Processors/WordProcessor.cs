﻿using System.Collections.Generic;
using WordOccurrenceCounter.Models;

namespace WordOccurrenceCounter.Processors
{
    public class WordProcessor : IWordProcessor
    {
        public WordOccurrenceModel ProcessWords(List<string> words)
        {
            var result = new WordOccurrenceModel();

            words.ForEach(x => result.Add(x, 1));

            return result;
        }
    }
}
