﻿using System.Collections.Generic;
using WordOccurrenceCounter.Models;

namespace WordOccurrenceCounter
{
    public interface IWordProcessor
    {
        WordOccurrenceModel ProcessWords(List<string> words);
    }
}