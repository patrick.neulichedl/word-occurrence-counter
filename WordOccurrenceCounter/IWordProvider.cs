﻿using System.Collections.Generic;

namespace WordOccurrenceCounter
{
    public interface IWordProvider
    {
        List<string> GetWords();
    }
}
