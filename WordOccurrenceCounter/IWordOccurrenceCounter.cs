﻿using System;
using WordOccurrenceCounter.Models;

namespace WordOccurrenceCounter
{
    public interface IWordOccurrenceCounter
    {
        TimeSpan ComputationTime { get; }

        WordOccurrenceModel CountWordOccurrences();

        WordOccurrenceStatisticsModel GetWordOccurrenceStatistics();
    }
}
