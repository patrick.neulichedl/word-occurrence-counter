﻿using System.Linq;
using System.Text;

namespace WordOccurrenceCounter.Models
{
    public class WordOccurrenceStatisticsModel
    {
        public int TotalWordCount { get; private set; }

        public string FirstWordWithHighestOccurrence { get; private set; }

        public string FirstWordWithLowestOccurrence { get; private set; }

        private WordOccurrenceStatisticsModel()
        {
        }

        public static WordOccurrenceStatisticsModel GetStatisticsModelFromWordOccurrences(
            WordOccurrenceModel wordOccurrenceModel)
        {
            var wordOccurrences = wordOccurrenceModel.WordOccurrences;

            if (wordOccurrences.Keys.Count > 0)
            {
                return new WordOccurrenceStatisticsModel()
                {
                    TotalWordCount = wordOccurrences.Select(x => x.Value).Sum(),
                    FirstWordWithHighestOccurrence = wordOccurrences.Aggregate((l, r) => l.Value < r.Value ? r : l).Key,
                    FirstWordWithLowestOccurrence = wordOccurrences.Aggregate((l, r) => l.Value > r.Value ? r : l).Key,
                };
            }

            return new WordOccurrenceStatisticsModel()
            {
                TotalWordCount = 0,
                FirstWordWithHighestOccurrence = string.Empty,
                FirstWordWithLowestOccurrence = string.Empty,
            };
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.Append("Statistics:").AppendLine();
            builder.AppendLine();
            builder.Append("Total word count: ").Append(TotalWordCount).AppendLine();
            builder.Append("First word with highest occurrence: ").Append(FirstWordWithHighestOccurrence).AppendLine();
            builder.Append("First word with lowest occurrence: ").Append(FirstWordWithLowestOccurrence).AppendLine();

            return builder.ToString();
        }
    }
}
