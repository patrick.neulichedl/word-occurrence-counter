﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WordOccurrenceCounter.Models
{
    public class WordOccurrenceModel
    {
        public Dictionary<string, int> WordOccurrences { get; }

        public WordOccurrenceModel()
            : this(new Dictionary<string, int>())
        {
        }

        public WordOccurrenceModel(Dictionary<string, int> wordOccurrences)
        {
            WordOccurrences = wordOccurrences ?? throw new ArgumentNullException(nameof(wordOccurrences));
        }

        public void Add(string word, int count)
        {
            if (!WordOccurrences.ContainsKey(word))
            {
                WordOccurrences.Add(word, 0);
            }

            WordOccurrences[word] += count;
        }

        public static WordOccurrenceModel operator +(WordOccurrenceModel left, WordOccurrenceModel right)
        {
            var result = new WordOccurrenceModel(left.WordOccurrences);

            foreach (var item in right.WordOccurrences)
            {
                result.Add(item.Key, item.Value);
            }

            return result;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is WordOccurrenceModel other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            if (WordOccurrences.Count != other.WordOccurrences.Count)
            {
                return false;
            }

            return !WordOccurrences.Except(other.WordOccurrences).Any();
        }

        public override int GetHashCode()
        {
            return -1142462288 + EqualityComparer<Dictionary<string, int>>.Default.GetHashCode(WordOccurrences);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.Append("Word occurrences:").AppendLine();
            builder.AppendLine();

            foreach (var item in WordOccurrences)
            {
                builder.Append(item.Key).Append(": ").Append(item.Value).AppendLine();
            }

            return builder.ToString();
        }
    }
}
