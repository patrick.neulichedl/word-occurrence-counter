﻿using System.Collections.Generic;
using WordOccurrenceCounter.Models;

namespace WordOccurrenceCounter.Counters
{
    public class DefaultWordOccurrenceCounter : AWordOccurrenceCounter
    {
        public DefaultWordOccurrenceCounter(IWordProcessor wordProcessor, List<IWordProvider> wordProviders)
            : base(wordProcessor, wordProviders)
        {
        }

        protected override WordOccurrenceModel ComputeWordOccurrences()
        {
            var result = new WordOccurrenceModel();

            foreach (var wordProvider in _wordProviders)
            {
                List<string> words = wordProvider.GetWords();
                result += _wordProcessor.ProcessWords(words);
            }

            return result;
        }
    }
}
