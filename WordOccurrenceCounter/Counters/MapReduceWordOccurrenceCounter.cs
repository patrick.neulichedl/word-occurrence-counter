﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WordOccurrenceCounter.Models;

namespace WordOccurrenceCounter.Counters
{
    public class MapReduceWordOccurrenceCounter : AWordOccurrenceCounter
    {
        public MapReduceWordOccurrenceCounter(IWordProcessor wordProcessor, List<IWordProvider> wordProviders)
            : base(wordProcessor, wordProviders)
        {
        }

        protected override WordOccurrenceModel ComputeWordOccurrences()
        {
            Task<WordOccurrenceModel> task = Task.Run(() => ComputeWordOccurrencesAsync());
            task.Wait();

            if (task.IsFaulted)
            {
                return null;
            }

            return task.Result;
        }

        private async Task<WordOccurrenceModel> ComputeWordOccurrencesAsync()
        {
            var unitsOfWork = new List<Task<WordOccurrenceModel>>();

            WordOccurrenceModel computationResult = new WordOccurrenceModel();

            foreach (var wordProvider in _wordProviders)
            {
                var unitOfWork = Task.Run(() =>
                {
                    List<string> words = wordProvider.GetWords();
                    return _wordProcessor.ProcessWords(words);
                });

                unitsOfWork.Add(unitOfWork);
            }

            WordOccurrenceModel[] computedModels = await Task.WhenAll(unitsOfWork);


            foreach (var wordOccurrenceModel in computedModels)
            {
                computationResult += wordOccurrenceModel;
            }

            return computationResult;
        }
    }
}
