﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using WordOccurrenceCounter.Models;

namespace WordOccurrenceCounter.Counters
{
    public abstract class AWordOccurrenceCounter : IWordOccurrenceCounter
    {
        protected readonly IWordProcessor _wordProcessor;
        protected readonly List<IWordProvider> _wordProviders;

        private WordOccurrenceModel _wordOccurrences;

        public TimeSpan ComputationTime { get; private set; }

        protected AWordOccurrenceCounter(IWordProcessor wordProcessor, List<IWordProvider> wordProviders)
        {
            _wordProcessor = wordProcessor ?? throw new ArgumentNullException(nameof(wordProcessor));
            _wordProviders = wordProviders ?? throw new ArgumentNullException(nameof(wordProviders));
        }

        public WordOccurrenceModel CountWordOccurrences()
        {
            // Word occurrences have already been counted
            if (_wordOccurrences != null)
            {
                return _wordOccurrences;
            }

            var timing = Stopwatch.StartNew();
            _wordOccurrences = ComputeWordOccurrences();
            timing.Stop();

            ComputationTime = timing.Elapsed;

            return _wordOccurrences;
        }

        public WordOccurrenceStatisticsModel GetWordOccurrenceStatistics()
        {
            if (_wordOccurrences == null)
            {
                throw new InvalidOperationException(
                    "Word occurrences must be calculated before requesting statistics data");
            }

            return WordOccurrenceStatisticsModel.GetStatisticsModelFromWordOccurrences(_wordOccurrences);
        }

        protected abstract WordOccurrenceModel ComputeWordOccurrences();
    }
}
