﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace WordOccurrenceCounter.Providers
{
    public class FileWordProvider : IWordProvider
    {
        private readonly string _filePath;

        public FileWordProvider(string filePath)
        {
            if (filePath is null)
            {
                throw new ArgumentNullException(nameof(filePath));
            }

            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException($"Unable to instantiate {nameof(FileWordProvider)}, file not found.", filePath);
            }

            _filePath = filePath;
        }

        public List<string> GetWords()
        {
            string text = File.ReadAllText(_filePath);

            text = Regex.Replace(text, @"[^\w]", " ");

            return text.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
