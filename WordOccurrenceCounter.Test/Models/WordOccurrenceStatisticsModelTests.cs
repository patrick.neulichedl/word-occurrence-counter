﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordOccurrenceCounter.Models;

namespace WordOccurrenceCounter.Test.Models
{
    [TestClass]
    public class WordOccurrenceStatisticsModelTests
    {
        [TestMethod]
        public void GetStatisticsModelFromWordOccurrenceShouldInitializeModelCorrectlyWithoutWordOccurrences()
        {
            // Arrange
            var wordOccurrences = new Dictionary<string, int>();

            var wordOccurrenceModel = new WordOccurrenceModel(wordOccurrences);

            // Act
            var statisticsModel = WordOccurrenceStatisticsModel
                .GetStatisticsModelFromWordOccurrences(wordOccurrenceModel);

            // Assert
            Assert.AreEqual(expected: 0, actual: statisticsModel.TotalWordCount);
            Assert.AreEqual(expected: string.Empty, actual: statisticsModel.FirstWordWithHighestOccurrence);
            Assert.AreEqual(expected: string.Empty, actual: statisticsModel.FirstWordWithLowestOccurrence);

        }

        [TestMethod]
        public void GetStatisticsModelFromWordOccurrenceShouldReturnModelWithCorrectTotalWordCount()
        {
            // Arrange
            var wordOccurrences = new Dictionary<string, int>()
            {
                { "hallo", 5 },
                { "peter", 1 },
            };

            var wordOccurrenceModel = new WordOccurrenceModel(wordOccurrences);

            // Act
            var statisticsModel = WordOccurrenceStatisticsModel
                .GetStatisticsModelFromWordOccurrences(wordOccurrenceModel);

            // Assert
            Assert.AreEqual(expected: 6, actual: statisticsModel.TotalWordCount);
        }

        [TestMethod]
        public void GetStatisticsModelFromWordOccurrenceShouldReturnModelWithCorrectFirstWordWithHighestOccurrence()
        {
            // Arrange
            var wordOccurrences = new Dictionary<string, int>()
            {
                { "word", 5 },
                { "occurrences", 1 },
                { "model", 5 },
            };

            var wordOccurrenceModel = new WordOccurrenceModel(wordOccurrences);

            // Act
            var statisticsModel = WordOccurrenceStatisticsModel
                .GetStatisticsModelFromWordOccurrences(wordOccurrenceModel);

            // Assert
            Assert.AreEqual(expected: "word", actual: statisticsModel.FirstWordWithHighestOccurrence);
        }

        [TestMethod]
        public void GetStatisticsModelFromWordOccurrenceShouldReturnModelWithCorrectFirstWordWithLowestOccurrence()
        {
            // Arrange
            var wordOccurrences = new Dictionary<string, int>()
            {
                { "word", 5 },
                { "occurrences", 1 },
                { "model", 1 },
            };

            var wordOccurrenceModel = new WordOccurrenceModel(wordOccurrences);

            // Act
            var statisticsModel = WordOccurrenceStatisticsModel
                .GetStatisticsModelFromWordOccurrences(wordOccurrenceModel);

            // Assert
            Assert.AreEqual(expected: "occurrences", actual: statisticsModel.FirstWordWithLowestOccurrence);
        }
    }
}
