﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordOccurrenceCounter.Models;

namespace WordOccurrenceCounter.Test.Models
{
    [TestClass]
    public class WordOccurrenceModelTests
    {
        [TestMethod]
        public void AddShouldInitializeOccurrenceForWordCorrectly()
        {
            // Arrange
            var wordOccurrenceModel = new WordOccurrenceModel();

            var expectedOutcome = new Dictionary<string, int>()
            {
                { "test", 5 },
            };

            // Act
            wordOccurrenceModel.Add("test", 5);

            // Assert
            CollectionAssert.AreEqual(expected: expectedOutcome, actual: wordOccurrenceModel.WordOccurrences,
                message: $"{nameof(WordOccurrenceModel.Add)} did not initialize occurrence for word correctly");
        }

        [TestMethod]
        public void AddShouldIncreaseWordOccurrenceCorrectly()
        {
            // Arrange
            var wordOccurrenceModel = new WordOccurrenceModel();

            var expectedOutcome = new Dictionary<string, int>()
            {
                { "test", 5 },
            };

            // Act
            wordOccurrenceModel.Add("test", 2);
            wordOccurrenceModel.Add("test", 3);

            // Assert
            CollectionAssert.AreEqual(expected: expectedOutcome, actual: wordOccurrenceModel.WordOccurrences,
                    message: $"{nameof(WordOccurrenceModel.Add)} did not increase occurrence for word correctly");
        }

        [TestMethod]
        public void EqualsShouldCompareModelsCorrectly()
        {
            // Arrange
            var firstWordOccurrenceModel = new WordOccurrenceModel(
                new Dictionary<string, int>()
                {
                    { "test", 5 },
                }
            );

            var secondWordOccurrenceModel = new WordOccurrenceModel(
                new Dictionary<string, int>()
                {
                    { "test", 5 },
                }
            );

            // Assert
            Assert.IsTrue(firstWordOccurrenceModel.Equals(secondWordOccurrenceModel));
        }

        [TestMethod]
        public void PlusEqualsShouldAddCorrectly()
        {
            // Arrange
            var firstWordOccurrenceModel = new WordOccurrenceModel(
                new Dictionary<string, int>()
                {
                    { "test", 5 },
                    { "a word", 1 },
                }
            );

            var secondWordOccurrenceModel = new WordOccurrenceModel(
                new Dictionary<string, int>()
                {
                    { "test", 5 },
                }
            );

            var expectedCombinedWordOccurrences =
                new Dictionary<string, int>()
                {
                    { "test", 10 },
                    { "a word", 1 },
                };

            // Act
            var combinedWordOccurrenceModel = firstWordOccurrenceModel += secondWordOccurrenceModel;

            // Assert
            CollectionAssert.AreEqual(expected: expectedCombinedWordOccurrences, actual: combinedWordOccurrenceModel.WordOccurrences,
                    message: $"+= did not add occurrence for words correctly");
        }

    }
}
