﻿using System.Collections.Generic;
using WordOccurrenceCounter.Models;

namespace WordOccurrenceCounter.Test.Extensions
{
    public static class ListExtensions
    {
        public static WordOccurrenceModel GetExpectedWordOccurrences(this List<string> words)
        {
            Dictionary<string, int> expectedWordOccurrences = new Dictionary<string, int>();

            foreach (var word in words)
            {
                // If this is the first occurrence of the word
                // add it to the expected word occurrences dictionary
                if (!expectedWordOccurrences.ContainsKey(word))
                {
                    // Initialized to zero, because the count is raised in the next statement
                    expectedWordOccurrences.Add(word, 0);
                }

                // Increase the occurrence count of this word by one
                expectedWordOccurrences[word]++;
            }

            return new WordOccurrenceModel(expectedWordOccurrences);
        }
    }
}
