﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WordOccurrenceCounter.Counters;
using WordOccurrenceCounter.Models;
using WordOccurrenceCounter.Test.Extensions;
using WordOccurrenceCounter.Test.Mocks;

namespace WordOccurrenceCounter.Test.Counters
{
    [TestClass]
    public class DefaultWordOccurrenceCounterTests
    {
        private WordProcessorMock _wordProcessorMock;
        private WordProviderMock _wordProviderMock;

        [TestInitialize]
        public void Setup()
        {
            _wordProcessorMock = new WordProcessorMock();
            _wordProviderMock = new WordProviderMock();
        }

        [TestCleanup]
        public void Teardown()
        {
            _wordProcessorMock = null;
            _wordProviderMock = null;
        }

        [TestMethod]
        public void ShouldCountWordOccurrencesCorrectly()
        {
            // Arrange
            List<string> words = new List<string> { "word1", "word1", "word2", "word3", "word3", "word3" };

            WordOccurrenceModel expectedWordOccurrences = words.GetExpectedWordOccurrences();

            _wordProcessorMock.SetupProcessWordsFor(expectedWordOccurrences);
            _wordProviderMock.SetupGetWordsReturning(words);

            var wordOccurrenceCounter = new DefaultWordOccurrenceCounter(
                _wordProcessorMock.Object,
                new List<IWordProvider>() { _wordProviderMock.Object }
            );

            // Act
            WordOccurrenceModel wordOccurrences = wordOccurrenceCounter.CountWordOccurrences();

            // Assert
            Assert.AreEqual(expected: expectedWordOccurrences, actual: wordOccurrences);

            _wordProcessorMock.Verify(x => x.ProcessWords(words), Times.Once,
                "Word occurrence counter did not use custom word processor");

            _wordProviderMock.Verify(x => x.GetWords(), Times.Once(),
                "Word occurrence counter did not use custom word provider");
        }

        [TestMethod]
        public void ResultShouldNotChangeWhenCountWordOccurrencesIsCalledTwice()
        {
            // Arrange
            List<string> words = new List<string> { "word1" };

            _wordProcessorMock.SetupProcessWordsFor(
                words.GetExpectedWordOccurrences()
            );
            _wordProviderMock.SetupGetWordsReturning(words);

            var wordOccurrenceCounter = new DefaultWordOccurrenceCounter(
                _wordProcessorMock.Object,
                new List<IWordProvider>() { _wordProviderMock.Object }
            );

            // Act
            WordOccurrenceModel wordOccurrencesFirstCall = wordOccurrenceCounter.CountWordOccurrences();
            WordOccurrenceModel wordOccurrencesSecondCall = wordOccurrenceCounter.CountWordOccurrences();

            // Assert
            Assert.AreEqual(expected: wordOccurrencesFirstCall, actual: wordOccurrencesSecondCall,
                message: $"{nameof(DefaultWordOccurrenceCounter.CountWordOccurrences)} " +
                            $"did return a different value on the second call.");
        }
    }
}
