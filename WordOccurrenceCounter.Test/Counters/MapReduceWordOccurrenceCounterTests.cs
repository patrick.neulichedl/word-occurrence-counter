﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WordOccurrenceCounter.Counters;
using WordOccurrenceCounter.Models;
using WordOccurrenceCounter.Test.Extensions;
using WordOccurrenceCounter.Test.Mocks;

namespace WordOccurrenceCounter.Test.Counters
{
    [TestClass]
    public class MapReduceWordOccurrenceCounterTests
    {
        private WordProcessorMock _wordProcessorMock;

        [TestInitialize]
        public void Setup()
        {
            _wordProcessorMock = new WordProcessorMock();

            // This workaround is needed due to Moq not being thread-safe when using SetupSequence
            _wordProcessorMock.Setup(x => x.ProcessWords(It.IsAny<List<string>>()))
                .Returns((List<string> words) => words.GetExpectedWordOccurrences());
        }

        [TestCleanup]
        public void Teardown()
        {
            _wordProcessorMock = null;
        }

        [TestMethod]
        public void ShouldCountWordOccurrencesCorrectly()
        {
            // Arrange
            List<string> wordsFirstProvider = new List<string> { "word1", "word1", "word2", "word3", "word3", "word3" };
            List<string> wordsSecondProvider = new List<string> { "word4", "word6", "word2", "word6", "word3", "word3" };

            List<WordProviderMock> wordProviderMocks = WordProviderMock.GetMocksFor(
                wordsFirstProvider,
                wordsSecondProvider
            );

            var wordOccurrenceCounter = new MapReduceWordOccurrenceCounter(
                _wordProcessorMock.Object,
                wordProviderMocks.Select(x => x.Object).ToList()
            );

            // Act
            WordOccurrenceModel wordOccurrences = wordOccurrenceCounter.CountWordOccurrences();

            // Assert
            Assert.AreEqual(expected: wordsFirstProvider.GetExpectedWordOccurrences() +
                    wordsSecondProvider.GetExpectedWordOccurrences(),
                actual: wordOccurrences);

            _wordProcessorMock.Verify(x => x.ProcessWords(wordsFirstProvider), Times.Once,
                "Words of first provider have not been processed");

            _wordProcessorMock.Verify(x => x.ProcessWords(wordsSecondProvider), Times.Once,
                "Words of second provider have not been processed");

            wordProviderMocks.ForEach(mock => mock.Verify(x => x.GetWords(), Times.Once()));
        }
    }
}
