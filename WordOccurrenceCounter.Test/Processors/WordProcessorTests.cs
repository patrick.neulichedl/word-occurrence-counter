﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordOccurrenceCounter.Processors;
using WordOccurrenceCounter.Test.Extensions;

namespace WordOccurrenceCounter.Test.Processors
{
    [TestClass]
    public class WordProcessorTests
    {
        [TestMethod]
        public void ShouldProcessWordsCorrectly()
        {
            // Arrange
            List<string> words = new List<string> { "word1", "word1", "word2", "word3", "word3", "word3" };

            var wordProcessor = new WordProcessor();

            var expectedOutcome = words.GetExpectedWordOccurrences();

            // Act
            var result = wordProcessor.ProcessWords(words);

            // Assert
            Assert.AreEqual(expected: expectedOutcome, actual: result);
        }
    }
}
