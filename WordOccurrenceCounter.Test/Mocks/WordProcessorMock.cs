﻿using System.Collections.Generic;
using Moq;
using WordOccurrenceCounter.Models;

namespace WordOccurrenceCounter.Test.Mocks
{
    public class WordProcessorMock : Mock<IWordProcessor>
    {
        public WordProcessorMock SetupProcessWordsFor(params WordOccurrenceModel[] wordOccurrenceModels)
        {
            var sequence = SetupSequence(x => x.ProcessWords(It.IsAny<List<string>>()));

            foreach (var wordOccurrenceModel in wordOccurrenceModels)
            {
                sequence.Returns(() => wordOccurrenceModel);
            }

            return this;
        }
    }
}
