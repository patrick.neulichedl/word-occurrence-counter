﻿using System.Collections.Generic;
using Moq;

namespace WordOccurrenceCounter.Test.Mocks
{
    public class WordProviderMock : Mock<IWordProvider>
    {
        public WordProviderMock SetupGetWordsReturning(List<string> words)
        {
            Setup(x => x.GetWords()).Returns(() => words);

            return this;
        }

        public static List<WordProviderMock> GetMocksFor(params List<string>[] wordParams)
        {
            var result = new List<WordProviderMock>();

            foreach (var words in wordParams)
            {
                result.Add(new WordProviderMock().SetupGetWordsReturning(words));
            }

            return result;
        }
    }
}
