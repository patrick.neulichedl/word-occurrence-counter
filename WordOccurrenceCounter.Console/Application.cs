﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WordOccurrenceCounter.ConsoleApp.Dialogs;
using WordOccurrenceCounter.ConsoleApp.Dialogs.choices;
using WordOccurrenceCounter.Counters;
using WordOccurrenceCounter.Models;
using WordOccurrenceCounter.Processors;
using WordOccurrenceCounter.Providers;

namespace WordOccurrenceCounter.ConsoleApp
{
    public static class Application
    {
        public static void Main(string[] args)
        {
            ClearScreen();

            List<string> filePaths = ShowInputFilePathSelection();
            List<IWordProvider> wordProviders = GetWordProvidersFrom(filePaths);

            ClearScreen();

            IWordProcessor wordProcessor = new WordProcessor();
            IWordOccurrenceCounter wordOccurrenceCounter = ShowWordOccurrenceCounterSelection(
                wordProcessor, wordProviders
            );

            ClearScreen();

            Console.WriteLine("Counting words, this may take some time...");

            WordOccurrenceModel wordOccurrences = wordOccurrenceCounter.CountWordOccurrences();
            TimeSpan computationTime = wordOccurrenceCounter.ComputationTime;
            WordOccurrenceStatisticsModel wordOccurrenceStatistics = wordOccurrenceCounter.GetWordOccurrenceStatistics();

            ClearScreen();

            OutputTypes outputType = OutputSelectionDialog.Execute();

            ClearScreen();

            Console.WriteLine($"Computed a total of {wordOccurrenceStatistics.TotalWordCount.ToString("N0")} words in {computationTime.Milliseconds}ms.");

            var resultString = GetStringRepresentationFrom(wordOccurrences, wordOccurrenceStatistics);
            OutputResult(resultString, outputType);

            Console.WriteLine();
            Console.WriteLine("Done. Press any key to exit.");
            Console.ReadKey();
        }

        private static string GetStringRepresentationFrom(
            WordOccurrenceModel wordOccurrences, WordOccurrenceStatisticsModel wordOccurrenceStatistics)
        {
            var textRepresentationBuilder = new StringBuilder();

            textRepresentationBuilder.Append(wordOccurrenceStatistics);
            textRepresentationBuilder.AppendLine();
            textRepresentationBuilder.Append(wordOccurrences);

            return textRepresentationBuilder.ToString();
        }

        private static void OutputResult(string resultString, OutputTypes outputType)
        {
            switch (outputType)
            {
                case OutputTypes.TextFile:
                    var filePath = FilePathInputDialog.Execute(".txt");

                    if (File.Exists(filePath))
                    {
                        var shouldOverride = BooleanInputDialog.Execute(
                            $"File '{filePath}' already exists, would you like to replace it?",
                            false
                        );

                        if (!shouldOverride)
                        {
                            ClearScreen();
                            OutputResult(resultString, outputType);
                            return;
                        }
                    }

                    File.WriteAllText(filePath, resultString);
                    break;
                default:

                    Console.WriteLine("Word occurrences for provided files, are as follows:");
                    Console.WriteLine(resultString);
                    break;
            }
        }

        private static IWordOccurrenceCounter ShowWordOccurrenceCounterSelection(
            IWordProcessor wordProcessor, List<IWordProvider> wordProviders)
        {
            WordOccurrenceCounterTypes wordOccurrenceCounterType = WordOccurrenceCounterSelectionDialog.Execute();

            switch (wordOccurrenceCounterType)
            {
                case WordOccurrenceCounterTypes.MapRecude:
                    return new MapReduceWordOccurrenceCounter(wordProcessor, wordProviders);
                default:
                    return new DefaultWordOccurrenceCounter(wordProcessor, wordProviders);
            }
        }

        private static List<string> ShowInputFilePathSelection()
        {
            List<string> filePaths = new List<string>();

            var wouldLikeToAddAnother = false;

            do
            {
                var filePath = FilePathInputDialog.Execute(".txt");

                if (!File.Exists(filePath))
                {
                    var retry = BooleanInputDialog.Execute(
                        $"Error file '{filePath}' not found, would you like to retry?",
                        true
                    );

                    if (!retry)
                    {
                        throw new FileNotFoundException("Unable to find specified file", filePath);
                    }

                    continue;
                }

                if (filePaths.Contains(filePath))
                {
                    var shouldAdd = BooleanInputDialog.Execute(
                        $"File '{filePath}' was already provided, would you like to add it anyways?",
                        false
                    );

                    if (shouldAdd)
                    {
                        filePaths.Add(filePath);
                    }
                }
                else
                {
                    filePaths.Add(filePath);
                }

                wouldLikeToAddAnother = BooleanInputDialog.Execute(
                    "Would you like to add another file path?",
                    true
                );

                ClearScreen();

            } while (wouldLikeToAddAnother);

            return filePaths;
        }

        private static void ShowWelcomeScreen()
        {
            Console.WriteLine("---------------------------");
            Console.WriteLine("   WordOccurrenceCounter   ");
            Console.WriteLine("---------------------------");
            Console.WriteLine();
        }

        private static void ClearScreen()
        {
            Console.Clear();
            ShowWelcomeScreen();
        }

        private static List<IWordProvider> GetWordProvidersFrom(List<string> filePaths)
        {
            List<IWordProvider> wordProviders = new List<IWordProvider>();

            wordProviders.AddRange(filePaths.Select(x => new FileWordProvider(x)).ToList());

            return wordProviders;
        }
    }
}
