﻿using System;
using WordOccurrenceCounter.ConsoleApp.Dialogs.choices;

namespace WordOccurrenceCounter.ConsoleApp.Dialogs
{
    internal static class OutputSelectionDialog
    {
        internal static OutputTypes Execute()
        {
            Console.WriteLine("How would you like to have your word occurrences displayed?");
            Console.WriteLine("In this window: [c]");
            Console.WriteLine("In a text document: t");

            if (Console.ReadLine().Equals("t"))
            {
                return OutputTypes.TextFile;
            }

            return OutputTypes.Console;
        }
    }
}
