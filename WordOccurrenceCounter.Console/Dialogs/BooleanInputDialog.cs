﻿using System;

namespace WordOccurrenceCounter.ConsoleApp.Dialogs
{
    internal static class BooleanInputDialog
    {
        internal static bool Execute(string question, bool defaultAnswer)
        {
            Console.Write(question);

            if (defaultAnswer == true)
            {
                Console.Write(" [yes]/no: ");
                return !Console.ReadLine().Equals("no");
            }
            else
            {
                Console.Write(" yes/[no]: ");
                return Console.ReadLine().Equals("yes");
            }
        }
    }
}
