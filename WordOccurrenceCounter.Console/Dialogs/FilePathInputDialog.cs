﻿using System;

namespace WordOccurrenceCounter.ConsoleApp.Dialogs
{
    internal static class FilePathInputDialog
    {
        internal static string Execute(string fileEnding)
        {
            Console.Write($"Please provide a file path, ending in '{fileEnding}': ");
            string filePath = Console.ReadLine();

            filePath = filePath.Trim('"');

            if (!filePath.EndsWith(fileEnding))
            {
                Console.WriteLine($"Specified file '{filePath}' has wrong file ending," +
                    $" expected '{fileEnding}'");
                return Execute(fileEnding);
            }

            return filePath;
        }
    }
}
