﻿using System;
using WordOccurrenceCounter.ConsoleApp.Dialogs.choices;

namespace WordOccurrenceCounter.ConsoleApp.Dialogs
{
    internal static class WordOccurrenceCounterSelectionDialog
    {
        internal static WordOccurrenceCounterTypes Execute()
        {
            Console.WriteLine("Which word occurrence counter would you like to use?");
            Console.WriteLine("Default: [d]");
            Console.WriteLine("Map-reduce: m");

            if (Console.ReadLine().Equals("m"))
            {
                Console.WriteLine("Using Map-reduce word occurrence counter.");
                return WordOccurrenceCounterTypes.MapRecude;
            }

            return WordOccurrenceCounterTypes.Normal;
        }
    }
}
